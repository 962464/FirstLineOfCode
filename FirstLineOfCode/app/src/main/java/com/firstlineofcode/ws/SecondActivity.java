package com.firstlineofcode.ws;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;

public class SecondActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
//        Intent i = getIntent();
//        Log.d("SecondActivity", i.getStringExtra("extra_data"));

        Button button = (Button) findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra("data_return", "hello firstactivity");
                setResult(RESULT_OK, i);
                finish();
//                ActivityCollector.finishAll();
//                android.os.Process.killProcess(android.os.Process.myPid());
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("SeconActivity", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("SeconActivity", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("SeconActivity", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("SeconActivity", "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("SeconActivity", "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("SeconActivity", "onDestroy");

    }

    /**
     * 自己定义好需要哪些参数，省去沟通、阅读代码的麻烦
     * **/
    public static void actionStart(Context context, String data1, String data2) {
        Intent intent = new Intent(context,SecondActivity.class);
        intent.putExtra("param1", data1);
        intent.putExtra("param2", data2);
        context.startActivity(intent);
    }

}
