package com.firstlineofcode.ws;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private ImageView imageview;
    private ProgressBar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            Log.d("MainActivity", savedInstanceState.getString("si_data"));
        }

        //隐藏actionBar
//        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null){
//            actionBar.hide();
//        }

        findViewById(R.id.button).setOnClickListener(this);
        imageview = (ImageView) findViewById(R.id.imageview);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button:
                //TODO implement
                int num = progressbar.getProgress();
                num += 10;
                progressbar.setProgress(num);

                imageview.setImageResource(R.mipmap.ic_launcher);

                AlertDialog.Builder ad = new AlertDialog.Builder(this);
                ad.setTitle("title").setMessage("message").setCancelable(false).setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();

                break;
        }
    }

    private EditText getEt() {
        return (EditText) findViewById(R.id.et);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MainActivity", "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity", "onDestroy");
    }

    //    加载Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    //    菜单项选中处理
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_item:
                Toast.makeText(this, "点击了Add", Toast.LENGTH_SHORT).show();
                break;
            case R.id.remove_item:
                Toast.makeText(this, "点击了Remove", Toast.LENGTH_SHORT).show();
                break;
            case R.id.finish_item:
                finish();
            case R.id.explicit_intent:
                // 显示Intent（直接指明我们要跳转的目标）
//                startActivity(new Intent(this, SecondActivity.class));

                // 隐式Intent（只有action和category同时能够匹配才能触发，android.intent.catefory.DEFAULT是一种默认的category，在startActivty时会自动添加）
//                Intent i = new Intent();
//                i.setAction("com.firstlineofcode.ws.IMPILCIT_INTENT");
//                i.addCategory("com.firstlinofcode.ws.MY_CATEGORY");
//                startActivity(i);

                //更多的隐式Intent的方法
//                Intent i = new Intent(Intent.ACTION_VIEW);//调用浏览器显示指定网页
//                i.setData(Uri.parse("http://www.baidu.com"));//将字符串所解析的Uri对象传递过去
//                startActivity(i);

//                Intent i = new Intent(Intent.ACTION_DIAL);//拨号
//                i.setData(Uri.parse("tel:1008611"));
//                startActivity(i);

                //向下一个Activity传递数据
//                Intent i = new Intent(this, SecondActivity.class);
//                i.putExtra("extra_data", "hello world");
//                startActivity(i);

                //接受返回的数据
                Intent i = new Intent(this, SecondActivity.class);
                startActivityForResult(i, 1);

                SecondActivity.actionStart(this, "data1", "data2");

                break;
            default:
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == RESULT_OK) {
                    Log.d("MainActivity", data.getStringExtra("data_return"));
                }
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("si_data", "我是被销毁时保存的数据");
    }
}
